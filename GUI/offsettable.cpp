#include "offsettable.h"
#include <QString>
#include <QMenu>
#include <QAction>
#include <unordered_map>

#include <QString>
#include <QHeaderView>

#include <iostream>
using namespace std;

#define ORDER_COL 0
#define NAME_COL 1
#define OFFSET_X_COL 2
#define OFFSET_Y_COL 3
#define OFFSET_Z_COL 4

OffsetTable::OffsetTable(QWidget *parent) :
    QTableWidget(parent)
{
    connect(this,SIGNAL(cellChanged(int,int)),this,SLOT(on_cell_changed(int,int)));
    horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

OffsetTable::~OffsetTable()
{

}


void OffsetTable::mousePressEvent(QMouseEvent *event)
{
    QModelIndex item = indexAt(event->pos());
    bool selected = selectionModel()->isSelected(item);
    QTableWidget::mousePressEvent(event);
    if ((item.row() == -1 && item.column() == -1) || selected)
    {
        clearSelection();
        const QModelIndex index;
        selectionModel()->setCurrentIndex(index, QItemSelectionModel::Select);
    }
}



void OffsetTable::updateOffset(){

    regionOffset.clear();

    for(int row = 0 ; row < rowCount(); row++){
        QTableWidgetItem* itemName = item(row,NAME_COL);
        if(itemName == nullptr || itemName->text().isEmpty()) break;

        string name = itemName->text().toStdString();
        array<float,3> offsets;

        bool isEmpty = false;
        for(int i = OFFSET_X_COL ; i < OFFSET_X_COL + 3 ; i++){
            QTableWidgetItem* itemOffset = item(row,i);
            if(itemOffset == nullptr || itemOffset->text().isEmpty()){
                isEmpty = true;
                break;
            }
            offsets[i - OFFSET_X_COL] = itemOffset->text().toFloat();
        }
        if(isEmpty) break;

        regionOffset.insert(make_pair(name,offsets));

//        cout << name << ": ";
//        for(int i = 0 ; i < 3; i++){
//            cout << "," << offsets[i];
//        }
//        cout << endl;
    }
}

void OffsetTable::updateOrder(){
    regionOrder.clear();

    for(int row = 0 ; row < rowCount(); row++){
        QTableWidgetItem* itemName = item(row,NAME_COL);
        if(itemName == nullptr || itemName->text().isEmpty()) break;

        string name = itemName->text().toStdString();

        QTableWidgetItem* itemOrder = item(row,ORDER_COL);
        int order = itemOrder->text().toInt();
        regionOrder.insert(make_pair(order,name));

    }
}

void OffsetTable::on_cell_changed(int row,int col ){
    if(row != -1 && col != -1){
        updateOffset();
        updateOrder();
    }

    //this->resizeColumnsToContents();
}

std::map<std::string,std::array<float,3>> OffsetTable::GetRegionOffset() const{
    return regionOffset;
}

std::map<int,string> OffsetTable::GetRegionOrder() const{
    return regionOrder;
}

void OffsetTable::initializeRegions(vector<string> regionNames){
    //Clear all rows
    while(rowCount()!= 0)
        removeRow(rowCount()-1);

    //Insert initial regions
    for(unsigned i = 0 ; i < regionNames.size(); i++){
        insertRow(i);

        QString qOrder;
        qOrder.sprintf("%d",i);

        setItem(i, ORDER_COL, new QTableWidgetItem(qOrder));
        setItem(i, NAME_COL,new QTableWidgetItem(QString(regionNames[i].c_str())));

        for (int j = OFFSET_X_COL;j <= OFFSET_Z_COL; j++){
            setItem(i, j , new QTableWidgetItem(QString('0')));
        }
    }

    updateOffset();
    updateOrder();

}
