#ifndef VTKWINDOW_H
#define VTKWINDOW_H

#include <QMainWindow>
#include <string>


#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkInteractionStyle)

#include <vtkUnstructuredGridReader.h>
#include <vtkActor.h>
#include <vtkOpenGLRenderer.h>
#include <vtkDataSetMapper.h>
#include <vtkGenericOpenGLRenderWindow.h>


#include "./VTK/Clipper.h"

namespace Ui {
class VTKWindow;
}

class VTKWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit VTKWindow(QWidget *parent = 0);
    ~VTKWindow();

    void readVTKFile(std::string);

private slots:
    void on_actionClipper_triggered();

private:
    Ui::VTKWindow *ui;

    vtkActor* actor;

    vtkOpenGLRenderer* renderer;

    vtkUnstructuredGridReader* reader;

    Clipper* clipper = nullptr;

};

#endif // VTKWINDOW_H
