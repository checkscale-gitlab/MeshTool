#include "filedialog.h"
#include "ui_filedialog.h"
#include <iostream>
#include <QFileDialog>
#include <QTextStream>
#include <QDataStream>

using namespace std;

QStringList FileDialog::filePaths;

FileDialog::FileDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileDialog)
{
    ui->setupUi(this);

    filePaths.clear();
}

FileDialog::~FileDialog()
{
    delete ui;
}


void FileDialog::on_buttonBox_accepted()
{

    QString s = ui->textEdit->toPlainText();
    filePaths = s.split("\n",QString::SkipEmptyParts);
//    QTextStream ss(stdout);
//    for(int i = 0 ; i < filePaths.size(); i++){
//        ss<< filePaths[i]<< "\n";
//    }
}


void FileDialog::on_buttonBox_rejected()
{
    filePaths.clear();
}


vector<string> FileDialog::GetInputFile(QWidget* parent)
{
    FileDialog dialog(parent);
    dialog.exec();

    vector<string> paths;
    for(int i=0; i < filePaths.size(); i++)
        paths.push_back(filePaths.at(i).toStdString());

    return paths;
}


void FileDialog::on_addButton_clicked()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this);

    for(int i = 0 ; i < fileNames.size() ;i++)
      ui->textEdit->append(fileNames.at(i));
}
