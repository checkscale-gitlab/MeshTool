#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
using namespace std;

#include <QFileDialog>
#include <QFile>
#include <QThread>
#include <QTimer>
#include <QTableWidgetItem>
#include <QTextStream>
#include <QProgressDialog>
#include <QFutureWatcher>
#include <QtConcurrent/QtConcurrent>

#include "Core/MeshStarter.hpp"
#include "Core/DirectoryHelper.hpp"
#include "Core/PolyMaths.hpp"

#include "CompareMeshDialog.hpp"
#include "MessageBoxUtils.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    enableSettings(false);
    ui->applyButton->setEnabled(false);
    ui->cellSize_lineEdit->setPlaceholderText("Single decimal or a comma separated list for each surface (e.g. '1.0,0.6,0.5')");
    //ui->actionVisualize->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionMesh_triggered()
{
    filePaths = FileDialog::GetInputFile(this);

    if(filePaths.size() == 0 ) return ;

    saveFilePath.clear();

    enableSettings(true);

    offSetTableSetup();

    ui->applyButton->setEnabled(true);
}


void MainWindow::enableSettings(bool enabled)
{
    ui->facetAngle_lineEdit->setEnabled(enabled);
    ui->facetSize_lineEdit->setEnabled(enabled);
    ui->facetDistance_lineEdit->setEnabled(enabled);
    ui->cellSize_lineEdit->setEnabled(enabled);
    ui->cellRadiusEdgeRatio_lineEdit->setEnabled(enabled);
    ui->smooth_lineEdit->setEnabled(enabled);
    ui->bboxMargin_xmax_lineEdit->setEnabled(enabled);
    ui->bboxMargin_ymax_lineEdit->setEnabled(enabled);
    ui->bboxMargin_zmax_lineEdit->setEnabled(enabled);
    ui->bboxMargin_xmin_lineEdit->setEnabled(enabled);
    ui->bboxMargin_ymin_lineEdit->setEnabled(enabled);
    ui->bboxMargin_zmin_lineEdit->setEnabled(enabled);
    ui->low_resolution_radio->setEnabled(enabled);
    ui->high_resolution_radio->setEnabled(enabled);
    ui->qualityAssess_check->setEnabled(enabled);
}

void MainWindow::on_applyButton_released()
{  
    if(filePaths.size() > 0 ){
//        if(saveFilePath.isEmpty())
            saveFilePath = QFileDialog::getSaveFileName(this);

        //If no save path is input, don't do anything
        if(saveFilePath.isEmpty())
            return ;
    }

    //Disable GUI when meshing
    this->setEnabled(false);

    // Reorder the surface order by the user input
    reOrderFilePaths(filePaths,ui->offsetTable->GetRegionOrder());

    // Start meshing
    start_mesh();

    // Re enabled the GUI
    ui->actionVisualize->setEnabled(true);
    this->setEnabled(true);
}


MeshProperty MainWindow::read_properties_from_lineEdits()
{
    float facetAngle = ui->facetAngle_lineEdit->text().toFloat();
    float facetSize = ui->facetSize_lineEdit->text().toFloat();
    float facetDistance = ui->facetDistance_lineEdit->text().toFloat();
    float cellRadiusEdgeRatio = ui->cellRadiusEdgeRatio_lineEdit->text().toFloat();
    int smoothFactor = ui->smooth_lineEdit->text().toInt();
    vector<float> dummyCellSize = {1.0};

    string cellSizeStr = ui->cellSize_lineEdit->text().toStdString();

    // use a dummy cellSize first
    MeshProperty tmp(facetAngle,facetSize,facetDistance,cellRadiusEdgeRatio,dummyCellSize,smoothFactor,read_bbox_margin());

    // override cellSize with list
    tmp.cellSizeFromString(cellSizeStr);

    return tmp;
}


void MainWindow::write_properties_to_lineEdits(MeshProperty property){

    QString qFacetAngle,qFacetSize,qFacetDistance,qCellRadiusEdgeRatio,qCellSize,qSmoothFactor;

    qFacetAngle.sprintf("%f",property.facetAngle);
    qFacetSize.sprintf("%f",property.facetSize);
    qFacetDistance.sprintf("%f",property.facetDistance);
    qCellRadiusEdgeRatio.sprintf("%f",property.cellRadiusEdgeRatio);
    qSmoothFactor.sprintf("%d",property.smoothFactor);

    // create the cellSize string list
    qCellSize = QString::fromStdString(property.cellSizeToString());

    ui->facetAngle_lineEdit->setText(qFacetAngle);
    ui->facetSize_lineEdit->setText(qFacetSize);
    ui->facetDistance_lineEdit->setText(qFacetDistance);
    ui->cellRadiusEdgeRatio_lineEdit->setText(qCellRadiusEdgeRatio);
    ui->cellSize_lineEdit->setText(qCellSize);
    ui->smooth_lineEdit->setText(qSmoothFactor);
}

void MainWindow::start_mesh(){
    QFileInfo info(QString::fromStdString(filePaths[0]));
    QString file_extension = info.suffix();

    //Read mesh Parameters
    MeshProperty property = read_properties_from_lineEdits();
    property.perRegionOffset = ui->offsetTable->GetRegionOffset();

    //Check if input files are all same type
    for(string s : filePaths){
        QFileInfo temp(QString::fromStdString(s));
        QString suffix = temp.suffix();
        if(suffix != file_extension){
            MessageBoxUtils::showErrorMessageBox(QString("Input files are not same type."),
                                                this);
            return ;
        }
    }

    // Create a progress dialog.
    QProgressDialog dialog(this);
    dialog.setLabelText(QString("Start meshing"));
    dialog.setCancelButton(0); // Disable the cancel button
    dialog.setWindowFlags(Qt::Drawer | Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint); //Disable the close and resize button

    // Create a QFutureWatcher and connect signals and slots.
    QFutureWatcher<bool> futureWatcher;
    QObject::connect(&futureWatcher, &QFutureWatcher<bool>::finished, &dialog, &QProgressDialog::reset);
    QObject::connect(&futureWatcher,  &QFutureWatcher<bool>::progressRangeChanged, &dialog, &QProgressDialog::setRange);
    QObject::connect(&futureWatcher, &QFutureWatcher<bool>::progressValueChanged,  &dialog, &QProgressDialog::setValue);

    struct Mesh_function_param {
        vector<string> filePaths;
        string fileType;
        string saveFilePath;
        MeshProperty property;
        bool enabledQA;
    };

    // Prepare the vector.
    QVector<Mesh_function_param> param_vec;
    Mesh_function_param m_param = {
        .filePaths = filePaths,
        .fileType = file_extension.toStdString(),
        .saveFilePath = saveFilePath.toStdString(),
        .property = property,
        .enabledQA = isQualityAssessmentEnabled()
    };
    param_vec.push_back(m_param);

    std::function<bool(Mesh_function_param&)> mesh_function = [](Mesh_function_param& param) {
        return mesh_start(param.filePaths, param.fileType, param.saveFilePath, param.property, param.enabledQA);
    };

    // Start the computation.
    futureWatcher.setFuture(QtConcurrent::run(mesh_function, m_param));

    // Display the dialog and start the event loop.
    dialog.exec();

    futureWatcher.waitForFinished();

    bool result = futureWatcher.result();
    if (result) {
        MessageBoxUtils::showInfoMessageBox(QString("Mesh is created successfully ! Please use Paraview to check the result."),
                                            this);
    } else {
        MessageBoxUtils::showErrorMessageBox(QString("Failed to create mesh! Please check the log information."),
                                            this);
    }
}


void MainWindow::offSetTableSetup(){
    vector<string> names;
    for(auto name : filePaths){
        names.push_back(basename(name));
    }
    ui->offsetTable->initializeRegions(names);
}



void MainWindow::on_actionVisualize_triggered()
{
    if(vtkWindow == nullptr){
        vtkWindow = new VTKWindow();
    }

    vtkWindow->readVTKFile((saveFilePath + ".mesh.vtk").toStdString());
    vtkWindow->show();
}


void MainWindow::on_actionSettings_triggered()
{
    QString path = QFileDialog::getOpenFileName(this);
    QFile setting(path);

    if (!setting.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&setting);

    MeshProperty m_property;
    QString line = in.readLine();
    while(!line.isEmpty()){
        QStringList ql = line.split(" ",QString::SkipEmptyParts);
        string property = ql.at(0).toStdString();
        cout << property << endl;
        line = in.readLine();

        float value = ql.at(1).toFloat();
        if(property == "facetAngle") {
            m_property.facetAngle = value;
        } else if(property == "facetSize") {
            m_property.facetSize = value;
        } else if(property == "facetDistance") {
            m_property.facetDistance = value;
        } else if(property == "cellSize") {
            // TYS: parse cell size different here, support a list here too
            m_property.cellSize.clear();
            m_property.cellSize.push_back(value);
        } else if(property == "cellRadiusEdgeRatio") {
            m_property.cellRadiusEdgeRatio = value;
        } else if(property == "smoothFactor") {
            m_property.smoothFactor = (int)value;
        }
    }

    write_properties_to_lineEdits(m_property);
    setting.close();
}

void MainWindow::on_actionAs_settings_triggered()
{
    QString outPath = QFileDialog::getSaveFileName(this);

    QFile out(outPath);
    if (!out.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

    QTextStream s(&out);

    //Read mesh parameters
    MeshProperty property = read_properties_from_lineEdits();

    s << "facetAngle " << property.facetAngle << endl;
    s << "facetSize " << property.facetSize << endl;
    s << "facetDistance " << property.facetDistance << endl;
    s << "cellRadiusEdgeRatio " << property.cellRadiusEdgeRatio << endl;

    // display list of cell sizes (could be single item)
    s << "cellSizes: " << QString::fromStdString(property.cellSizeToString()) << endl;

    out.close();

}


void MainWindow::reOrderFilePaths(vector<string>& filePaths,const map<int,string> regionOrder){
    vector<string> copy;

    for(auto iter = regionOrder.begin(); iter != regionOrder.end(); iter ++){
        for(unsigned i = 0 ; i < filePaths.size(); i++){
            if(iter->second == basename(filePaths[i]))
                copy.push_back(filePaths[i]);
        }
    }

    filePaths = copy;
}

bool MainWindow::isQualityAssessmentEnabled(){
    return ui->qualityAssess_check->isChecked();
}


array<double,6> MainWindow::read_bbox_margin(){
    array<double,6> margin;

    margin[0] = ui->bboxMargin_xmin_lineEdit->text().toDouble();
    margin[1] = ui->bboxMargin_ymin_lineEdit->text().toDouble();
    margin[2] = ui->bboxMargin_zmin_lineEdit->text().toDouble();
    margin[3] = ui->bboxMargin_xmax_lineEdit->text().toDouble();
    margin[4] = ui->bboxMargin_ymax_lineEdit->text().toDouble();
    margin[5] = ui->bboxMargin_zmax_lineEdit->text().toDouble();

    return margin;
}

void MainWindow::on_actionMesh_Compare_triggered()
{
    pair<string,string> files = CompareMeshDialog::GetFiles(this);
    if(!files.first.empty() && !files.second.empty())
        compareUnstructuredGrid(files.first,files.second);
}


void MainWindow::on_low_resolution_radio_clicked()
{
    vector<float> cellSizeResLow = {1.0};
    ui->high_resolution_radio->setChecked(false);
    MeshProperty low_resolution(25,5.0,0.1,1.5,cellSizeResLow,200,{0,0,0,0,0,0});
    write_properties_to_lineEdits(low_resolution);
}

void MainWindow::on_high_resolution_radio_clicked()
{
    vector<float> cellSizeResHigh = {0.7};
    ui->low_resolution_radio->setChecked(false);
    MeshProperty high_resolution(25,3.0,0.07,1.5,cellSizeResHigh,200,{0,0,0,0,0,0});
    write_properties_to_lineEdits(high_resolution);
}
