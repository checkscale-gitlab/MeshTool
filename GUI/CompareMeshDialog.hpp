#ifndef COMPAREMESHDIALOG_HPP
#define COMPAREMESHDIALOG_HPP

#include <QDialog>
#include <string>

namespace Ui {
class CompareMeshDialog;
}

class CompareMeshDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CompareMeshDialog(QWidget *parent = 0);
    ~CompareMeshDialog();

    static std::pair<std::string,std::string> GetFiles(QWidget* parent);
    static std::string refPath;
    static std::string testPath;

private slots:
    void on_buttonBox_accepted();

    void on_addTest_button_released();

    void on_addRef_button_released();

private:
    Ui::CompareMeshDialog *ui;

};

#endif // COMPAREMESHDIALOG_HPP
