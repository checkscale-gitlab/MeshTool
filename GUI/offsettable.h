#ifndef OFFSETTABLE_H
#define OFFSETTABLE_H

#include <QWidget>
#include <QTableWidget>
#include <QFocusEvent>
#include <QTableWidgetItem>

#include <map>
#include <string>
#include <array>
class OffsetTable : public QTableWidget
{
    Q_OBJECT

public:
    explicit OffsetTable(QWidget *parent = 0);
    ~OffsetTable();

    std::map<std::string,std::array<float,3>> GetRegionOffset() const;
    std::map<int,std::string> GetRegionOrder() const;

    void initializeRegions(std::vector<std::string>);

private:
    virtual void mousePressEvent(QMouseEvent *event);

    std::map<std::string,std::array<float,3>> regionOffset;
    std::map<int,std::string> regionOrder;

    void updateOffset();

    void updateOrder();


private slots:
    void on_cell_changed(int ,int );

};

#endif // OFFSETTABLE_H
