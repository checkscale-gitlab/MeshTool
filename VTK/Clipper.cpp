#include "Clipper.h"
#include "implicitPlaneCallBack.h"

#include <vtkAlgorithmOutput.h>

Clipper::Clipper()
{
//    plane = vtkSmartPointer<vtkPlane>::New();
//    implicitPlane = vtkSmartPointer<vtkImplicitPlaneWidget>::New();
//    extractGeometry = vtkSmartPointer<vtkExtractGeometry>::New();
//    clipDataset = vtkSmartPointer<vtkClipDataSet>::New();
}


void Clipper::createClipper(vtkActor* actor,
        vtkRenderWindowInteractor* interactor,
        vtkUnstructuredGrid* ug){

    plane = vtkSmartPointer<vtkPlane>::New();
    implicitPlane = vtkSmartPointer<vtkImplicitPlaneWidget>::New();
    extractGeometry = vtkSmartPointer<vtkExtractGeometry>::New();
    clipDataset = vtkSmartPointer<vtkClipDataSet>::New();

    double* scalarRange = ug->GetScalarRange();

    //set up for implicit plane
    implicitPlane->SetInteractor(interactor);
    implicitPlane->SetPlaceFactor(1.25);

    implicitPlane->PlaceWidget(actor->GetBounds());
    implicitPlane->SetOrigin(actor->GetCenter());

    implicitPlane->SetNormalToXAxis(1);

    implicitPlane->OutlineTranslationOff();
    implicitPlane->OriginTranslationOff();
    implicitPlane->DrawPlaneOff();

    //Crinkle clip set up
    extractGeometry->SetInputData(ug);
    extractGeometry->SetImplicitFunction(plane);
    extractGeometry->Update();

//    clipDataset->SetInputData(ug);
//    clipDataset->SetClipFunction(plane);
//    clipDataset->Update();

    //Set mapper
    vtkSmartPointer<vtkDataSetMapper> tempMapper
            = vtkSmartPointer<vtkDataSetMapper>::New();
    tempMapper->SetInputConnection(extractGeometry->GetOutputPort());
    tempMapper->SetScalarRange(scalarRange);
    tempMapper->Update();
    actor->SetMapper(tempMapper);


    //Set call back
    vtkSmartPointer<VTKImplicitPlaneWidgetCall> pCall
            = vtkSmartPointer<VTKImplicitPlaneWidgetCall>::New();
    pCall->setPlane(plane);
    pCall->setDataset(extractGeometry);

    implicitPlane->AddObserver(vtkCommand::EndInteractionEvent,pCall);

    implicitPlane->On();
}


void Clipper::Delete(){
    implicitPlane->Off();
    implicitPlane = nullptr;
    extractGeometry = nullptr;
    plane = nullptr;
}

