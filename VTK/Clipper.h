#ifndef CLIPPER_H
#define CLIPPER_H


#include <vtkActor.h>
#include <vtkDataSet.h>
#include <vtkClipDataSet.h>

#include <vtkCommand.h>
#include <vtkSmartPointer.h>
#include <vtkImplicitPlaneWidget.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkPlane.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataSetMapper.h>
#include <vtkProperty.h>
#include <vtkExtractGeometry.h>

class Clipper
{
public:
    Clipper();

    void createClipper(vtkActor* actor,
            vtkRenderWindowInteractor* interactor,
            vtkUnstructuredGrid* ug);

    void Delete();

private :

    vtkSmartPointer<vtkExtractGeometry> extractGeometry;

    vtkSmartPointer<vtkClipDataSet> clipDataset;

    vtkSmartPointer<vtkPlane> plane;

    vtkSmartPointer<vtkImplicitPlaneWidget> implicitPlane;
};

#endif // CLIPPER_H
