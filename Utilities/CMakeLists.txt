ADD_EXECUTABLE(surface_crop surface_crop.cpp)
TARGET_LINK_LIBRARIES(surface_crop ${VTK_LIBRARIES} ${Boost_PROGRAM_OPTIONS_LIBRARIES} ${Boost_FILESYSTEM_LIBRARIES})
 
ADD_TEST(NAME reg_surface_crop_tongue183_box
	COMMAND surface_crop --aabb -20,80,-50:20,130,-25
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00001.vtk
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00004.vtk
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00005.vtk
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00007.vtk
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00008.vtk
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00009.vtk
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00010.vtk
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00011.vtk)
		
ADD_TEST(NAME reg_surface_crop_tongue183_sphere
	COMMAND surface_crop --sphere -35,105,-30:20.0 --output-suffix ".sphere"
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00004.vtk
		${CMAKE_SOURCE_DIR}/meshes/183Tongue/183EditedTongueMesh00005.vtk)
		
INSTALL(TARGETS surface_crop
	DESTINATION bin)