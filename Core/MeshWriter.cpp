#include "MeshWriter.hpp"

#include <vtkDataReader.h>

#include <iostream>
#include <fstream>

#include "VTK/VTKLegacyReader.hpp"

using namespace std;

MeshWriter::MeshWriter()
{

}


void MeshWriter::writeMesh(string in,string outPath){
    ///Check if the file is type of unstructured grid
    vtkDataReader* reader = vtkDataReader::New();
    reader->SetFileName(in.c_str());

    if(!reader->IsFileUnstructuredGrid()){
        cout << "File is not unstructured grid " << endl;
        cout << "Fail to convert mesh file " << endl;
        return ;
    }

    reader->Delete();

    cout << "Read file from " << in << endl;
    string of = outPath + ".mesh";

    ofstream outMesh;
    outMesh.open(of.c_str());

    VTKLegacyReader R ;
    R.setFileName(in);

    TetraMeshBase tMesh = R.mesh();

    //The first two lines are number of points and tetras
    outMesh << tMesh.getNp()+1 << endl;
    outMesh << tMesh.getNt()+1 << endl;

    //Write out the points
    for(unsigned i = 0 ; i < tMesh.getNp() + 1; i++ ){
        auto point = tMesh.getPoint(i);
        outMesh << point[0] << " " << point[1] << " " << point[2] << endl;
    }

    //Write out the tetras
    for(unsigned i = 0 ; i < tMesh.getNt() + 1; i++){
        auto tetraPoints = tMesh.getTetraPointIDs(i);
        //Four vertexes and the material
        outMesh << tetraPoints[0] << " " << tetraPoints[1] << " " << tetraPoints[2]
                             << " " << tetraPoints[3] << " " << tMesh.getMaterial(i) << endl;
    }

    outMesh.close();

    cout << "Write file to " << outPath << ".mesh" << endl;
    cout << "Done" << endl;
    return;
}
