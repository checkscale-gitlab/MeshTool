#ifndef MESHQUALITY_HPP
#define MESHQUALITY_HPP

#include <vector>
#include <math.h>
#include <map>
#include <string>

#include "DirectoryHelper.hpp"

void meshQualityAssess(std::string in, std::map<int,std::string> regionOrder);

void meshQualityAssess(std::string in);

#endif // MESHQUALITY_HPP
