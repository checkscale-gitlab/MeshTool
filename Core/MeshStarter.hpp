#ifndef MESHSTARTER_HPP
#define MESHSTARTER_HPP

#include <string>
#include <vector>
using namespace std;

#include <boost/log/trivial.hpp>

#include "Mesher.hpp"
#include "CGALMesher.hpp"

bool mesh_start(vector<string> filePaths, string fileType, string saveFilePath, MeshProperty property, bool enabledQA) {
    Mesher * mesher;
    BOOST_LOG_TRIVIAL(trace) << "Start meshing";

    if (fileType == "vtk") {
        BOOST_LOG_TRIVIAL(info) << "File type is " << fileType;

        mesher = new CGALMesher();
        if (!mesher->read(filePaths)) {
            return false;
        }

        if (!mesher->mesh(property)) {
            return false;
        }

        if (!mesher->writeToVtkUnstructuredGrid(saveFilePath)) {
            return false;
        }

        BOOST_LOG_TRIVIAL(trace) << "Done" << endl;

        if (enabledQA) {
            mesher->qualityAccess();
        }

    } else {
        BOOST_LOG_TRIVIAL(error) << "Unknown file type";
        BOOST_LOG_TRIVIAL(trace) << "Failed" << endl;;
        return false;
    }

    return true;
}

#endif 
