#include "VTKPolyToCGAL.hpp"
#include "./VTK/VTKLegacyReader.hpp"

#include "DirectoryHelper.hpp"

#include <boost/log/trivial.hpp>

#include <vtkCell.h>

#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;


typedef pair<vector<array<float,3>>,vector<array<unsigned,3>>> PT;


pair<vector<array<float,3>>,vector<array<unsigned,3>>> VTKPolyToPT(string inputFile,int smoothFactor){
    VTKLegacyReader R;
    return R.readSurface(inputFile.c_str(),smoothFactor);
}


std::pair<std::vector<std::array<float,3>>,std::vector<std::array<unsigned,3>>> VTKPolyToPT(vtkPolyData* pd){

    bool m_addZeroPoint = false;
    bool m_addZeroCell = false;

    vector<array<float,3>> P(pd->GetNumberOfPoints()+(m_addZeroPoint?1:0));
    vector<array<unsigned,3>> T(pd->GetNumberOfPolys()+(m_addZeroCell?1:0));

    if (m_addZeroPoint)
        P[0] = array<float,3>{0,0,0};

    if(m_addZeroCell)
        T[0] = array<unsigned,3>{0,0,0};


    for(unsigned i=0;i<pd->GetNumberOfPoints();++i)
    {
        double *p = pd->GetPoint(i);
        std::copy(p, p+3, P[i+m_addZeroPoint].data());
    }

    for(unsigned i=0;i<pd->GetNumberOfPolys();++i)
    {
        vtkCell* c = pd->GetCell(i);
        if (c->GetNumberOfPoints() != 3)
            BOOST_LOG_TRIVIAL(error) << "Expecting 3 points in a cell, received " << c->GetNumberOfPoints();
        else
            for(unsigned j=0;j<3;++j)
                T[i+m_addZeroCell][j] = c->GetPointId(j)+m_addZeroPoint;
    }

    return make_pair(P,T);
}
