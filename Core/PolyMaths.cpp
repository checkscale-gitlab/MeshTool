#include "PolyMaths.hpp"
#include <fstream>
#include <iostream>
using namespace std;

#include <vtkDataSetSurfaceFilter.h>
#include <vtkBooleanOperationPolyDataFilter.h>
#include <vtkMassProperties.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkTriangleFilter.h>
#include <vtkUnstructuredGridReader.h>
#include "DirectoryHelper.hpp"

static double GetVolume(vtkPolyData* p);

void compareUnstructuredGrid(string test,string ref){
    cout << " =============== Comparison Results =============== " << endl;
    cout << "test mesh : " << basename(test) << endl;
    cout << "reference mesh : " << basename(ref) << endl;

    vtkSmartPointer<vtkUnstructuredGridReader> readTest =
            vtkSmartPointer<vtkUnstructuredGridReader>::New();
    readTest->SetFileName(test.c_str());
    readTest->Update();

    vtkSmartPointer<vtkUnstructuredGridReader> readRef =
            vtkSmartPointer<vtkUnstructuredGridReader>::New();
    readRef->SetFileName(ref.c_str());
    readRef->Update();

    double diff = volumeDiffBetweenMesh(readTest->GetOutput(),readRef->GetOutput());
    cout << "The difference between two meshes is " << diff << endl;

    cout << " =============== Comparison Done =============== " << endl;

}

double volumeDiffBetweenMesh(vtkUnstructuredGrid* test, vtkUnstructuredGrid* ref ){
    vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter1 =
            vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter2 =
            vtkSmartPointer<vtkDataSetSurfaceFilter>::New();

    cout << "Getting the surfaces of two meshes" << endl;
    surfaceFilter1->SetInputData(test);
    surfaceFilter2->SetInputData(ref);
    surfaceFilter1->Update();
    surfaceFilter2->Update();

    return  volumeDiffBetweenSurface(surfaceFilter1->GetOutput(),surfaceFilter2->GetOutput());
}


double volumeDiffBetweenSurface(vtkPolyData* test, vtkPolyData* ref ){
    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanPoly
            = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();

    booleanPoly->SetOperationToIntersection();

    booleanPoly->SetInputData(0,test);
    booleanPoly->SetInputData(1,ref);

    cout << "Getting the intersection of two meshes " << endl;
    booleanPoly->Update();

    double testVol = GetVolume(test);
    cout << "The volume of test mesh is : " << testVol << endl;

    double refVol = GetVolume(ref);
    cout << "The volume of reference mesh is : " << refVol << endl;

    double interVol = GetVolume(booleanPoly->GetOutput());
    cout << "The volume of intersection is : " << interVol << endl;

    return  (testVol - interVol)/refVol;
}

static double GetVolume(vtkPolyData* p){
    vtkSmartPointer<vtkMassProperties> property
            = vtkSmartPointer<vtkMassProperties>::New();

    vtkSmartPointer<vtkTriangleFilter> triFilter
            = vtkSmartPointer<vtkTriangleFilter>::New();

    triFilter->SetInputData(p);
    triFilter->Update();

    property->SetInputConnection(triFilter->GetOutputPort());
    property->Update();

    return property->GetVolume();
}

